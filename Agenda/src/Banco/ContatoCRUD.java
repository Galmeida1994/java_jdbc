package Banco;

import Sistema.Caderno;
import Sistema.Contato;
import java.sql.Types;
import java.util.ArrayList;


public class ContatoCRUD extends MeuDAO{
	
    public void inserir(Contato con,Caderno cad) throws Exception{
        openDatabase();
        String sql = "INSERT INTO contato (fk_id_Caderno,nome,telefoneFixo,telefoneCelular,endereco,cidade,dataNacimento,email,observacao) "
                + "VALUES (?,?,?,?,?,?,?,?,? )";
        pstmt = com.prepareStatement(sql);
        pstmt.setLong(1,cad.getId());
        pstmt.setString(2,con.getNome());
        pstmt.setString(3,con.getTelFixo());
        pstmt.setString(4,con.getTelCel());
        pstmt.setString(5,con.getEnd());
        pstmt.setString(6,con.getCidade());
        if(con.getDataNac() != null)
            pstmt.setDate(7,new java.sql.Date(con.getDataNac().getTime()));
        else
            pstmt.setNull(7,Types.DATE);
        pstmt.setString(8,con.getEmail());
        pstmt.setString(9,con.getObservacao());
        pstmt.execute();
        clouseDatabase();
    }

    public ArrayList<Contato> lerTudo(Caderno c)throws Exception{
        ArrayList<Contato> contato = new ArrayList();
        openDatabase();
        String sql = "SELECT * FROM contato WHERE contato.fk_id_Caderno =?";
        pstmt  = com.prepareStatement(sql);
        pstmt.setLong(1,c.getId());
        rs  = pstmt.executeQuery(); 
        while (rs.next()){
            Contato cad = new Contato();
            cad.setIdContato(rs.getLong("idContato"));
            cad.setIdFkCaderno(rs.getLong("fk_id_Caderno"));
            cad.setNome(rs.getString("nome"));
            cad.setTelFixo(rs.getString("telefoneFixo"));
            cad.setTelCel(rs.getString("telefoneCelular"));
            cad.setEnd(rs.getString("endereco"));
            cad.setCidade(rs.getString("cidade"));
            cad.setDataNac(rs.getDate("dataNacimento"));
            cad.setEmail(rs.getString("email"));
            cad.setObservacao(rs.getString("observacao"));

            contato.add(cad);
        }
        clouseDatabase();
        return contato;
    }
    
    public ArrayList<Contato> lerFiltro(Caderno c,String campo, String valor) throws Exception{
        ArrayList<Contato> contato = new ArrayList();
        openDatabase();
        String sql = "SELECT * FROM contato WHERE contato.fk_id_Caderno =? AND "+campo+" LIKE '%"+valor+"%'";
        pstmt  = com.prepareStatement(sql);
        pstmt.setLong(1,c.getId());
        rs  = pstmt.executeQuery(); 

        while (rs.next()){
        Contato cad = new Contato();
        cad.setIdContato(rs.getLong("idContato"));
        cad.setIdFkCaderno(rs.getLong("fk_id_Caderno"));
        cad.setNome(rs.getString("nome"));
        cad.setTelFixo(rs.getString("telefoneFixo"));
        cad.setTelCel(rs.getString("telefoneCelular"));
        cad.setEnd(rs.getString("endereco"));
        cad.setDataNac(rs.getDate("dataNacimento"));
        cad.setEmail(rs.getString("email"));
        cad.setObservacao(rs.getString("observacao"));

        contato.add(cad);
        }
        
        clouseDatabase();
        return contato;
    }   

    public void atualizar(Contato con,Caderno cad) throws Exception{
        openDatabase();
        String sql = "UPDATE contato set "
                + "nome = ?, telefoneFixo = ?, telefoneCelular = ?, endereco = ?, cidade = ?, dataNacimento = ?, email = ?, observacao = ?"
                + "  WHERE contato.fk_id_Caderno = ? AND contato.idContato = ?";
        pstmt  = com.prepareStatement(sql);
        pstmt.setString(1,con.getNome());
        pstmt.setString(2,con.getTelFixo());
        pstmt.setString(3,con.getTelCel());
        pstmt.setString(4,con.getEnd());
        pstmt.setString(5,con.getCidade());
        if(con.getDataNac() != null)
            pstmt.setDate(6,new java.sql.Date(con.getDataNac().getTime()));
        else
            pstmt.setNull(6,Types.DATE);
        pstmt.setString(7,con.getEmail());
        pstmt.setString(8,con.getObservacao());
        pstmt.setLong(9,cad.getId());
        pstmt.setLong(10,con.getIdContato());
        pstmt.execute();        
        clouseDatabase();
    }

    public void excluir(Contato con,Caderno cad) throws Exception{
        openDatabase();
        String sql = "DELETE FROM contato WHERE fk_id_Caderno = ? AND idContato = ?";
        pstmt  = com.prepareStatement(sql);
        pstmt.setLong(1,cad.getId());
        pstmt.setLong(2,con.getIdContato());
        pstmt.execute();        
        clouseDatabase();
    }
	
}