package Banco;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;


public class MeuDAO {
    public Connection com = null;
    public PreparedStatement pstmt;
    public Statement stmt;
    public ResultSet rs;
    private static final String USUARIO = "root";
    private static final String SENHA = "";
    private static final String URL = "jdbc:mysql://localhost/agenda";
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String TABELACADERNO = "CREATE TABLE Caderno ("
    +"idCaderno integer      AUTO_INCREMENT "
    			+"PRIMARY KEY "
                          +"NOT NULL, "                        
    +"nome      VARCHAR (81) NOT NULL "
                           +"UNIQUE); ";
    private static final String TABELACONTATO = "CREATE TABLE Contato ("
    +"idContato integer   AUTO_INCREMENT "                
    			+"PRIMARY KEY "
                          +"NOT NULL, "
    +"fk_id_Caderno   integer    NOT NULL, "
    +"foreign key(fk_id_Caderno) references caderno(idCaderno), "
    +"nome VARCHAR (81) NOT NULL, "
    +"telefoneFixo    VARCHAR (14), "
    +"telefoneCelular VARCHAR (15), "
    +"endereco         VARCHAR (81), "
    +"cidade	    VARCHAR (81), "
    +"dataNacimento   DATE, "
    +"email  	VARCHAR (81), "
    +"observacao  VARCHAR (511)); ";
    
	
    public void criateDatabase() throws Exception{
        try{
        Class.forName(DRIVER);
        com = DriverManager.getConnection ("jdbc:mysql://localhost/?user=root&password=");  
        stmt=com.createStatement(); 
        stmt.executeUpdate("CREATE DATABASE agenda");
        com.close();

        Class.forName(DRIVER);
        com = DriverManager.getConnection(URL, USUARIO, SENHA);
        stmt = com.createStatement();
        stmt.executeUpdate(TABELACADERNO);
        stmt.executeUpdate(TABELACONTATO);
        clouseDatabase();
        openDatabase();
        
        }catch(Exception ex){
            ex.printStackTrace();
        }     
    }
    
    
    public void openDatabase() throws Exception{
        try{
        Class.forName(DRIVER);
        com = DriverManager.getConnection(URL, USUARIO, SENHA);
        }catch(Exception ex){
            //ex.printStackTrace();
            clouseDatabase();
            criateDatabase();
        }
    }

    public void clouseDatabase() throws Exception{
        if(com != null){
            com.close();
        }
    }
}
